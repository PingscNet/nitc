﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NICTProject.Data.StoreProcedure;
using NICTProject.Services;
using NICTProject.ViewModel;

namespace NICTProject.Controllers
{
    public class ContactController : Controller
    {
        private readonly IDapperServices _idapperservices;

        public ContactController(IDapperServices idapperServices)
        {
            _idapperservices = idapperServices;
        }
        public IActionResult Index()
        {
            HomePageViewModel ViewModelHomePage = new HomePageViewModel();
            ViewModelHomePage.News = _idapperservices.GetNewsAndEvents(new GetEvents() { });
            return View(ViewModelHomePage);
        }

        public IActionResult Inquiry()
        {
            return View();
        }
    }
}
