﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NICTProject.Data.StoreProcedure;
using NICTProject.Services;
using NICTProject.ViewModel;

namespace NICTProject.Controllers
{
    public class UserController : Controller
    {
        private readonly IDapperServices _idapperservices;

        public UserController(IDapperServices idapperServices)
        {
            _idapperservices = idapperServices;
        }
        public IActionResult Login()
        {
            HomePageViewModel ViewModelHomePage = new HomePageViewModel();
            ViewModelHomePage.News = _idapperservices.GetNewsAndEvents(new GetEvents() { });
            return View(ViewModelHomePage);
        }
        
        public IActionResult Exam()
        {
            HomePageViewModel ViewModelHomePage = new HomePageViewModel();
            ViewModelHomePage.News = _idapperservices.GetNewsAndEvents(new GetEvents() { });
            return View(ViewModelHomePage);
        }

    }
}
