﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NICTProject.Data.StoreProcedure;
using NICTProject.Models;
using NICTProject.Services;
using NICTProject.ViewModel;

namespace NICTProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDapperServices _idapperservices;

        public HomeController(IDapperServices idapperServices)
        {
            _idapperservices = idapperServices;
        }

        public IActionResult Index()
        {
            HomePageViewModel ViewModelHomePage = new HomePageViewModel();
            ViewModelHomePage.News = _idapperservices.GetNewsAndEvents(new GetEvents() { });
            return View(ViewModelHomePage);
        }

        public IActionResult GolsAirms()
        {
            return View();
        }

        public IActionResult Gallery()
        {
            return View();
        }

    }
}
