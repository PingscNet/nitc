﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NICTProject.Data.StoreProcedure;
using NICTProject.Helper;
using NICTProject.Services;
using NICTProject.ViewModel;

namespace NICTProject.Controllers
{
    public class StudentVerificationController : Controller
    {
        private readonly IDapperServices _idapperservices;

        public StudentVerificationController(IDapperServices idapperServices)
        {
            _idapperservices = idapperServices;
        }

        public IActionResult Index()
        {
            HomePageViewModel ViewModelHomePage = new HomePageViewModel();
            ViewModelHomePage.News = _idapperservices.GetNewsAndEvents(new GetEvents() { });
            return View(ViewModelHomePage);
        }

        [HttpPost]
        public IActionResult StudentVerify(string EnrollmentNo)
        {
            AjaxResult response = new AjaxResult();
            response.Data = _idapperservices.GetStudentDetails(new GetStudentDetails() {Id = EnrollmentNo, BranchId = 0 });
            return Json(response);
        }
    }
}
