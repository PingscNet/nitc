﻿using Microsoft.AspNetCore.Mvc;
using NICTProject.Data.StoreProcedure;
using NICTProject.Services;
using NICTProject.ViewModel;
using Swashbuckle.Swagger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace NICTProject.Controllers
{
    public class BranchController : Controller
    {
        private readonly IDapperServices _idapperservices;

        public BranchController(IDapperServices idapperServices)
        {
            _idapperservices = idapperServices;
        }
        public IActionResult Index()
        {
            HomePageViewModel ViewModelHomePage = new HomePageViewModel();
            ViewModelHomePage.News = _idapperservices.GetNewsAndEvents(new GetEvents() { });
            return View(ViewModelHomePage);
        }
        
        public IActionResult BranchLanding()
        {
            return View();
        }

        public IActionResult GetStudents()
        {
            var StudentData = _idapperservices.GetStudentDetails(new GetStudentDetails() { BranchId = 40080 });
            return View();
        }

        public IActionResult GetFeesDetails()
        {
            return View();
        }

        public ActionResult DownloadExcel()
        {
            var StudentData = _idapperservices.GetStudentDetails(new GetStudentDetails() { BranchId = 40080 });
            XmlDocument xmlDoc = new XmlDocument();

            XmlSerializer xmlSerializer = new XmlSerializer(StudentData.GetType());

            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, StudentData);
                xmlStream.Position = 0;
                xmlDoc.Load(xmlStream);
            }

            var fName = string.Format("StudentsDetails-{0}.xls", DateTime.Now.ToString("s"));

            byte[] fileContents = Encoding.UTF8.GetBytes(xmlDoc.InnerXml);

            return File(fileContents, "application/vnd.ms-excel", fName);
        }

        public IActionResult UploadExcel()
        {
            return View();
        }
    }
}
