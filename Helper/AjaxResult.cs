﻿using System.Net;

namespace NICTProject.Helper
{
    public class AjaxResult
    {
        public string SuccessMessage { get; set; }
        public string ErrorMessage { get; set; }
        public string WarningMessage { get; set; }
        public int StatusCode { get; set; } = (int)HttpStatusCode.OK;
        public string View { get; set; }
        public object Data { get; set; }
        public string RedirectUrl { get; set; }
    }
}
