﻿using System;

namespace NICTProject.Helper
{
    [AttributeUsage(AttributeTargets.Property)]
    public class IgnoreParameterAttribute : Attribute
    {
    }
}
