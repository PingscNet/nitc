﻿using System;

namespace NICTProject.Helper
{
    [AttributeUsage(AttributeTargets.Class)]
    public class StoreProcAttribute : Attribute
    {
        private string Name;
        public StoreProcAttribute(string name)
        {
            this.Name = name;
        }

        internal string GetStoreProcName()
        {
            return Name;
        }
    }

}
