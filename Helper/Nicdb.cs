﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static NICTProject.Helper.StoreProcParamAttribute;

namespace NICTProject.Helper
{
    public class Nicdb
    {
        public static List<TOut> Get<TIn, TOut>(TIn type, string dbConnectionString)
        {
            Dictionary<string, object> keyValuePairs = null;
            string spname;
            GetSpNameAndParams(type, out spname, out keyValuePairs);
            CommandType commandType = CommandType.StoredProcedure;
            using IDbConnection db = new SqlConnection(dbConnectionString);
            return db.Query<TOut>(spname, keyValuePairs, commandType: commandType).ToList();
        }

        private static void GetSpNameAndParams(object type, out string spName, out Dictionary<string, object> spParams)
        {
            spName = null;
            spParams = null;
            StoreProcAttribute attribute = type.GetType()
                          .GetCustomAttributes(true)
                          .OfType<StoreProcAttribute>()
                          .FirstOrDefault();
            if (attribute != null)
            {
                spName = attribute.GetStoreProcName();
                spParams = new Dictionary<string, object>();
                foreach (var param in type.GetType().GetProperties())
                {
                    StoreProcParamAttribute attr = param
                          .GetCustomAttributes(true)
                          .OfType<StoreProcParamAttribute>()
                          .FirstOrDefault();
                    if (attr != null)
                    {
                        string paramName = attr.GetParamterName();
                        dynamic paramValue = attr.GetDefaultValue();
                        spParams.Add(string.IsNullOrEmpty(paramName) ? param.Name : paramName, paramValue != null ? paramValue : param.GetValue(type));
                    }

                }
            }
        }
    }
}
