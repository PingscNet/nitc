﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NICTProject.Helper
{
    [AttributeUsage(AttributeTargets.Property)]
    public class StoreProcParamAttribute : Attribute
    {
        private string ParameterName;
        private dynamic DefaultValue { get; set; }

        public StoreProcParamAttribute()
        {

        }
        public StoreProcParamAttribute(string parameterName)
        {
            this.ParameterName = parameterName;
        }
        public StoreProcParamAttribute(object defaultVaule)
        {
            this.DefaultValue = defaultVaule;
        }
        public StoreProcParamAttribute(string parameterName, object defaultVaule)
        {
            this.ParameterName = parameterName;
            this.DefaultValue = defaultVaule;
        }

        public object GetDefaultValue()
        {
            return DefaultValue;
        }

        public string GetParamterName()
        {
            return ParameterName;
        }
    }

}
