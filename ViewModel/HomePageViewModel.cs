﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NICTProject.ViewModel
{
    public class HomePageViewModel
    {
        public List<NewsAndEvent> News = new List<NewsAndEvent>();
        public List<NewsAndEvent> Events = new List<NewsAndEvent>();
    }
}
