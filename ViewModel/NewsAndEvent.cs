﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NICTProject.ViewModel
{
    public class NewsAndEvent
    {
        public int Id { get; set; }

        public string title { get; set; }
        public DateTime eventdate { get; set; }

        public string eventshortdesc { get; set; }

        public string eventdesc { get; set; }

        public bool active { get; set; }

        public bool ishome { get; set; }
    }
}
