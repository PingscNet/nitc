﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NICTProject.Data.StoreProcedure
{
    public class StudentDetails
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string  Address { get; set; }
        public string MobileNo { get; set; }
        public string CertificateNo { get; set; }
        public string Cource { get; set; }
        public bool IsActive { get; set; }
        public string Branch { get; set; }
        public int TotalFees { get; set; }
    }
}
