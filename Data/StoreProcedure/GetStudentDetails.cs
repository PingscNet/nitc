﻿using NICTProject.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NICTProject.Data.StoreProcedure
{
    [StoreProc("sp_StudentDetailVerification")]
    public class GetStudentDetails
    {
        [StoreProcParam]
        public string Id { get; set; }

        [StoreProcParam]
        public long BranchId { get; set; }
    }
}
