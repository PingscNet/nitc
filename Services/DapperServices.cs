﻿using Microsoft.Extensions.Configuration;
using NICTProject.Data.StoreProcedure;
using NICTProject.Helper;
using NICTProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NICTProject.Services
{
    public class DapperServices : IDapperServices
    {
        private readonly IConfiguration _config;
        private string Connectionstring = "DefaultConnection";

        public DapperServices(IConfiguration config)
        {
            _config = config;
        }

        public void GetCourse()
        {
        }

        public void GetMenu(GetMenu getmenu)
        {
        }

        public List<NewsAndEvent> GetNewsAndEvents(GetEvents getevent)
        {
           return Nicdb.Get<GetEvents, NewsAndEvent>(getevent, _config.GetConnectionString(Connectionstring));
        }

        public List<StudentDetails> GetStudentDetails(GetStudentDetails getstudentdetails)
        {
            return Nicdb.Get<GetStudentDetails, StudentDetails>(getstudentdetails, _config.GetConnectionString(Connectionstring)).ToList();
        }
    }
}
