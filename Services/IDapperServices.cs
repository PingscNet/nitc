﻿using NICTProject.Data.StoreProcedure;
using NICTProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NICTProject.Services
{
    public interface IDapperServices
    {
        List<NewsAndEvent> GetNewsAndEvents(GetEvents getevents);
        void GetMenu(GetMenu getmenu);
        void GetCourse();
        List<StudentDetails> GetStudentDetails(GetStudentDetails getstudentdetails);
    }
}
